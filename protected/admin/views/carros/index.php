<?php
/* @var $this CarrosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Carroses',
);
/*
$this->menu=array(
	array('label'=>'Create Carros', 'url'=>array('create')),
	array('label'=>'Manage Carros', 'url'=>array('admin')),
);*/
?>
<div class="container" style=" margin-top: 70px;">
    
  <h1>Carros</h1>


  
  <?php echo CHtml::link('Nuevo Carro',array('/carros/create'),array('class'=>'btn btn-success btn-lg')); ?>

  
  <table class="table">
       <thead>
        <tr>
            <th> Id </th>
            
            <th> Marca </th>
            <th> Modelo </th>
            <th> Precio </th>
              <th> Tipo </th>
                <th> Operacion </th>
           
         
     
        
           
        </tr>
    </thead>   
   <?php foreach($carros as $carro) { ?>  
  <tbody>
 <tr>
       <td style="padding-top: 20px; width: 10px;">
                   <ul class="nav nav-pills nav-stacked">
                       <span class="badge" style=" margin-top: 10px;  background-color:  #006dcc;"><?php echo $carro->id_carro;?></span>
                   </ul>
                 
             </td>
           
             <td style="padding-top: 30px;">
                <?php echo $carro->marca; ?>
            </td>
               <td style="padding-top: 30px;">
                <?php echo $carro->modelo; ?>
            </td>
               <td style="padding-top: 30px;">
                <?php echo $carro->precio; ?>
            </td>
               <td style="padding-top: 30px;">
                <?php echo $carro->tipo; ?>
            </td>
            
              <td style="padding-top: 20px; width: 10px;">
                   <ul class="nav nav-pills nav-stacked">
                       <li class="active">    <?php  echo CHtml::link('Ver',array('view','id'=>$carro->id_carro),array("style"=>"padding-left: 20px; padding-right: 20px;"));?> </li>
                   </ul>
             
            </td>
             <td style="padding-top: 20px; width: 10px;">
                  <ul class="nav nav-pills nav-stacked">
                       <li class="active">   <?php  echo CHtml::link('Editar',array('update','id'=>$carro->id_carro),array("style"=>"padding-left: 20px; padding-right: 20px;"));?></li>   
                   </ul>
             
            </td>
            <td style="padding-top: 20px; width: 10px;">
               <ul class="nav nav-pills nav-stacked">
                       <li class="active">    <?php  echo CHtml::link('Eliminar',array('delete','id'=>$carro->id_carro),array('confirm'=>'Esta seguro que desea borrar?'));?></li>   
                   </ul>
                 
            </td>
     
             <td style="padding-top: 10px; width: 15px;  ">
              
                  <a href=" <?php echo $this->createUrl('enable',array('id'=>$carro->id_carro));  ?>  ">
                      <h3>    <span style=" padding-left: 20px; padding-right: 20px; padding: 9px;" class="label label-<?php echo $carro->estatus==1?"info":"warning"; ?>">
                               <?php echo $carro->estatus==1?"Habilitado":"Desabilitado"; ?>
                           </span></h3>
                           
                                        </a> 
                 
            </td>
           
  
  
 </tr> 
 </tbody>
   <?php } ?>
</table>

  
  
  <?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'header'=>'',
    'footer'=>'',
    'nextPageLabel'=>"Siguiente",
    'prevPageLabel'=>"Anterior",
    'firstPageLabel'=>"Primera",
    'lastPageLabel'=>"Ultima",
    'selectedPageCssClass'=>'active',
    'hiddenPageCssClass'=>'disable',
    'htmlOptions'=>array('class'=>'pagination','style'=>'margin-left:380px'),
    
)); ?>
  
  
</div>

