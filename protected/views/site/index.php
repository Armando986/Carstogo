<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

/*
 * 
 <html< carousel bootstrap
 *   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

 
  
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/banner1.jpg"; ?>" alt="" >
      <div class="carousel-caption">
        
      </div>
    </div>
    <div class="item">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/bannerconst.jpg"; ?>" alt="" >
      <div class="carousel-caption">
     
      </div>
    </div>
      
        <div class="item">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/banner_office.jpg"; ?>" alt="" >
      <div class="carousel-caption">
     
      </div>
    </div>
  
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
 * 
 */

?>

<style type="text/css">


.caption-style-1 li:hover .caption{
opacity: 4;

}
.caption-style-1 img{
margin: 0px;
padding: 0px;
float: left;
z-index: 4;
width: 100%;
}
.caption-style-1 .caption{
cursor: pointer;
position: absolute;
opacity: 0;
-webkit-transition:all 0.25s ease-in-out;
-moz-transition:all 0.25s ease-in-out;
-o-transition:all 0.25s ease-in-out;
-ms-transition:all 0.25s ease-in-out;
transition:all 0.25s ease-in-out;
}
.caption-style-1 .blur{
background-color: rgba(0,0,0,0.68);
height: 200px;
width: 400px;
z-index: 5;
position: absolute;
}



.caption-style-1 .caption-text h1{
text-transform: uppercase;
font-size: 24px;
}
.caption-style-1 .caption-text {
z-index: 10;
color:  #fff;
position: absolute;
top: 40px;
text-align: center;
height: 200px;
width: 350px;
}

.caption-style-1 .caption-text a:hover a:active {
    color: white;
}


.caption-style-2 li:hover .caption{
opacity: 4;

}
.caption-style-2 img{
margin: 0px;
padding: 0px;
float: left;
z-index: 4;
width: 100%;
}
.caption-style-2 .caption{
cursor: pointer;
position: absolute;
opacity: 0;
-webkit-transition:all 0.25s ease-in-out;
-moz-transition:all 0.25s ease-in-out;
-o-transition:all 0.25s ease-in-out;
-ms-transition:all 0.25s ease-in-out;
transition:all 0.25s ease-in-out;
}
.caption-style-2 .blur{
background-color: rgba(0,0,0,0.68);
height: 103px;
width: 1140px;
z-index: 5;
position: absolute;
}

.caption-style-2 .caption-text h1{
text-transform: uppercase;
font-size: 24px;
}
.caption-style-2 .caption-text {
z-index: 10;
color:  #fff;
position: absolute;
margin-top: 27px;
text-align: center;
height: 100px;

margin-left:90px;
width: 1000px;
}

.caption-style-2 .caption-text a:hover a:active {
    color: white;
}

/** Nav Menu */
ul.nav-menu{
padding: 0px;
margin: 0px;
list-style-type: none;
width: 490px;
margin: 60px auto;
}
ul.nav-menu li{
display: inline;
margin-right: 10px;
padding:10px;
border: 1px solid #ddd;
}
ul.nav-menu li a{
color:  white;
text-decoration: none;
text-transform: uppercase;
}
ul.nav-menu li a:hover, ul.nav-menu li a.active{
color:   #fff;
}

</style>
<div class="container">

    <a href="#" class="scrollToTop"></a>
    <a href="#" class="scrollToDown"></a>
    

				<ul id="sb-slider" class="sb-slider">
					<li>
						<a href="#cut"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin1.jpg"; ?>"  /></a>
						
					</li>
					<li>
						<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin8.png"; ?>" /></a>
						
					</li>
					<li>
						<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin5.jpg"; ?>" /></a>
					</li>
                                        
                                        <li>
						<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin4.jpg"; ?>" /></a>
					</li>
                                        
                                        <li>
						<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin2.jpg"; ?>" /></a>
					</li>
                                         <li>
						<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin6.jpg"; ?>" /></a>
					</li>
					
				</ul>

				<!--
				<div id="nav-arrows" class="nav-arrows">
					<a href="#">Next</a>
					<a href="#">Previous</a>
				</div>-->
<!--
				<div id="nav-dots" class="nav-dots">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>-->

			
<!--
   <div class="box_skitter box_skitter_large">
	<ul>
		<li>
			<a href="#cut"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin1.jpg"; ?>" class="cut" /></a>
			<!-- <div class="label_text"><p>cut</p></div>
		</li>
		<li>
			<a href="#swapBlocks"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin2.jpg"; ?>" class="swapBlocks" /></a>
			
		</li>
		<li>
			<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin4.jpg"; ?>" class="swapBarsBack" /></a>
			
		</li>
                
                <li>
			<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin5.jpg"; ?>" class="swapBarsBack" /></a>
			
		</li>
                
                <li>
			<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin6.jpg"; ?>" class="swapBarsBack" /></a>
			
		</li>
                  <li>
			<a href="#swapBarsBack"><img src="<?php echo Yii::app()->request->baseUrl."/images/banner_fin8.png"; ?>" class="swapBarsBack" /></a>
			
		</li>
	</ul>
</div>
-->
    <br>
    <br>

   <!-- 
    
     <div class="alert alert-success" role="alert">
        
         <form class="form-inline" role="form">
  <div class="form-group">
    <div class="input-group">
      <label class="sr-only" for="exampleInputEmail2">Email address</label>
      <div class="input-group-addon">@</div>
      <input type="email" class="form-control input-lg" id="exampleInputEmail2" placeholder="Enter email">
    </div>
  </div>
  
  
  <button type="submit" class="btn btn-primary btn-lg">Sign in</button>
</form>
         
         
      </div>
   -->
   
    <div class="row">
        
    
    
        <div class="col-lg-4 caption-style-1">
       <center>    <div class="row">
            <div class="col-lg-12">
                
            
             <h4> <?php echo CHtml::link('Exportación de vehículos',array('site/page?view=about')); ?>   </h4>   </h4>
            <small><p style=" text-align: justify; ">Estamos radicados en los Estados Unidos, nos dedicamos a la exportación de vehículos nuevos y unidades comerciales. </p>
          </small>
            
            
            <!-- <a href="page?view=about"  class="grow"><img style="margin-top: 10px; margin-bottom: 10px;" src="<?php echo Yii::app()->request->baseUrl."/images/ban1.png" ?>" /></a>-->
           
 <?php //echo CHtml::link('Solicita tu cotización',array('/site/contact'),array('class'=>'btn btn-success')); ?>  
            </div>
              </div>
            <div class="row">
                <div class="col-lg-12">
            <li>
<img src="<?php echo Yii::app()->request->baseUrl."/images/ban1.png" ?>" alt="">
<div class="caption">
<div class="blur"></div>
<div class="caption-text">
<a href="site/page?view=about" style="color: white; text-decoration:none; "> <h3> Conoce nuestra empresa </h3>
<p>Click aqui </a>
</div>
</div>
</li>
                </div>
        </div>
           
        </div>
     <div class="col-lg-4 caption-style-1">
        <center>   <div class="row">
         <div class="col-lg-12">
            <h4> <?php echo CHtml::link('Pasos para comprar',array('site/pasos')); ?>   </h4>
          <small><p style=" text-align: justify; ">Quieres conocer como comprar tu vehiculo nuevo desde cualquier parte del mundo sin venir a los Estados Unidos. </p>
          </small>
         </div>
          </div>
          <div class="row">
         <div class="col-lg-12">
        <li>
            <img src="<?php echo Yii::app()->request->baseUrl."/images/ban2.png" ?>" alt="" style=" text-align: center">
<div class="caption">
<div class="blur" style=" width: 1200px;">
<div class="caption-text" >
    <a href="site/pasos" style="color: white; text-decoration:none; "> <h3>Pasos para comprar</h3>
<p>  Click Aqui</p> </a>
</div>
</div>
    </div>
</li>
         </div>
          </div>
     </div>
    
      <div class="col-lg-4 caption-style-1">
          
           <center>         <div class="row">
         <div class="col-lg-12">
                 <h4> <?php echo CHtml::link('Contactos',array('site/contactos')); ?>   </h4>
              <small><p style=" text-align: justify; ">Siguenos en nuestras redes sociales e informate de los nuevos modelos de vehículos que puedes adquirir desde tu país. </p>
          </small>
               <!-- <a href="contactos" class="grow">     <img style="margin-top: 18px; margin-bottom: 10px;" src="<?php echo Yii::app()->request->baseUrl."/images/ban3.png" ?>" />
        </a> -->
         </div></div>
          
              <div class="row">
         <div class="col-lg-12">
                      <li>
 <img src="<?php echo Yii::app()->request->baseUrl."/images/ban3.png" ?>" alt="">
<div class="caption">
<div class="blur"></div>
<div class="caption-text">
<a href="site/contactos" style="color: white; text-decoration:none; "> <h3>Contactanos</h3>
<p>Click Aqui </p> </a>
</div>
</div>
                 </li>
     </div>
              </div>
      </div>
    
    </div>   
    
         <div class="linea"></div>
    
        <div class="row">
            <div class="col-lg-12">
                 <div class="well"  style=" color:  #333333"> <h4> Contamos con amplio stock de vehículos de las diferentes y más reconocidas marcas, ofrecemos unidades comerciales usadas (Camiones y Maquinaria Pesada), ofrecemos los mejores precio para exportar vehículos desde los Estados Unidos, ahorrate los impuestos,
                    nosotros te gestionamos toda la documentación 
                    para que disfrutes de un vehículo nuevo. </h4></div>
                
            </div>
           
        </div>
        
         
        
        <div class="row" style="margin-top: 10px; margin-bottom: 20px;">
            <div class="col-lg-4">
                <center>
<div class="fb-like-box" data-href="https://www.facebook.com/pages/Car-Cars-to-Go-Corporation/288166734706073?ref=ts&amp;fref=ts" data-height="360" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
                </center> 
                
                
            </div>
             <div class="col-lg-4">
<!-- INSTANSIVE WIDGET --><script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/c3ca616284279dffb9884ca65d3bb2253344f963.html" id="instansive_c3ca616284" name="instansive_c3ca616284"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
            </div>
             <div class="col-lg-4">
                 <center>
                     
                     
               
              <a class="twitter-timeline" href="https://twitter.com/carstogocorp" data-widget-id="546028049578606594">Tweets por el @carstogocorp.</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
           
               </center>
             </div>
            
            
            </div>
         
        
            
         <center>  <div class="col-lg-12 caption-style-2">
          
          
          
              <div class="row">
       
                      <li>
                          <img src="<?php echo Yii::app()->request->baseUrl."/images/bannerusados.jpg" ?>" />
<div class="caption">
<div class="blur"></div>
<div class="caption-text">
    <a href="site/usados" style="color: white; text-decoration:none; margin-left: 40px;  "> <br> <h3> <<< Click Aqui >>> </h3></a>
</div>
</div>
                 </li>
     
              </div>
      </div>
         <br><br><br><br>
           <div class="linea"></div>
     
           <div class="row" style="margin-bottom: 13px;">
             
               
             <div class="col-lg-12">
                 <center>     <h5> Estamos publicado en las principales ciudades de Venezuela a traves de </h5>
                <img src="<?php echo Yii::app()->request->baseUrl."/images/oferta.png"; ?>"/>
                <a target="_blank" href="http://issuu.com/laofertaylademanda/docs/edicion_76_caracas"> Ver edición impresa </a> / <a target="_blank" href="http://www.laofertaylademanda.com/directorio/Estados-Unidos/Todos/car%20and%20cars">  Website </a>
                 </center>
             </div>   
            
        </div>
           
     
        
     <!--   
        <div class="row">
            
            <div id="logoParade">
                
                <div class="scrollWrapper">
                    
                
                <div class="scrollableArea">
                    <a target="_blank" href="#">
                        <img src="<?php echo Yii::app()->request->baseUrl."/images/ford.png"; ?>" width="190" height="80" />
                    </a>
                     <a target="_blank" href="#">
                         <img src="<?php echo Yii::app()->request->baseUrl."/images/chevro.png"; ?>" width="190" height="80" />
                    </a>
                     <a target="_blank" href="#">
                       <img src="<?php echo Yii::app()->request->baseUrl."/images/nissan.png"; ?>" width="100" height="90" />
                    </a>
                     <a target="_blank" href="#">
                       <img src="<?php echo Yii::app()->request->baseUrl."/images/fiat.png"; ?>" width="100" height="90" />
                    </a>
                     <a target="_blank" href="#">
                        <img src="<?php echo Yii::app()->request->baseUrl."/images/bwm.png"; ?>" width="90" height="90" />
                    </a>
                     <a target="_blank" href="#">
                       <img src="holder.js/200x100"/>
                    </a>
                     <a target="_blank" href="#">
                          <img src="holder.js/200x100"/>
                    </a>
                      <a target="_blank" href="#">
                          <img src="holder.js/200x100"/>
                    </a>
                      <a target="_blank" href="#">
                          <img src="holder.js/200x100"/>
                    </a>
                </div>
                    
               
                </div>
                
            </div>
        </div>
        -->

        
	

</div>


<script>
    
  
    
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56757233-1', 'auto');
  ga('send', 'pageview');

    
</script>