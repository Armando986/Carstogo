<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';

?>
<div class="container" style="margin-top: 80px;">
<div class=" page-header">
   <h2>Solicita tu cotización</h2> 
</div>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
Por favor llene el siguiente formulario para solicitar la cotización del vehículo, y en la brevedad posible te estaremos respondiendo.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

<div class="container">
	
<div class="row">
	<div class="row">
            
            <div class="col-md-6">
            
                <?php echo $form->labelEx($model,'Nombre - Apellido *'); ?>
		<?php echo $form->textField($model,'nombre',array('class'=>'form-control','style'=>'width: 450px;')); ?>
		<?php echo $form->error($model,'nombre'); ?>
                
                
            </div>
        </div>    
            
        <div class="row">
             <div class="col-md-6">
		<?php echo $form->labelEx($model,'Email *'); ?>
		<?php echo $form->textField($model,'email',array('class'=>'form-control','style'=>'width: 450px;')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
        </div>
		
	

	  <div class="row">

	 <div class="col-md-6">
		<?php echo $form->labelEx($model,'Vehículo *'); ?>
		<?php echo $form->textField($model,'titulo',array('class'=>'form-control','style'=>'width: 450px;','size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'titulo'); ?>
	</div>
          </div>
     <div class="row">

	 <div class="col-md-6">
		<?php echo $form->labelEx($model,'Puerto *'); ?>
		<?php echo $form->dropDownList($model,'puerto', array('NO SELECCIONO'=>'SELECCIONE','GUANTA'=>'GUANTA','LA GUAIRA'=>'LA GUAIRA','PUERTO CABELLO'=>'PUERTO CABELLO'),array('class'=>'form-control','style'=>'width: 200px;'));?>
		<?php echo $form->error($model,'puerto'); ?>
             
             
	</div>
          </div>
        
        
<div class="row">
	 <div class="col-md-6">
		<?php echo $form->labelEx($model,'Mensaje *'); ?>
		<?php echo $form->textArea($model,'mensaje',array('class'=>'form-control','style'=>'width: 550px','rows'=>10, 'cols'=>50)); ?>
		<?php echo $form->error($model,'mensaje'); ?>
	</div>
</div>
	<?php if(CCaptcha::checkRequirements()): ?>
    <div class="row">
    
       <div class="col-sm-12">
       <?php echo $form->labelEx($model,'verifyCode'); ?>
  
    </div>
        <div class="col-sm-12">
       <div style="float: left; margin-left: 0px;">
            <?php $this->widget('CCaptcha'); ?>
       </div>
        <div style="float: left; margin-left: 10px;">
    
      <?php echo $form->textField($model,'verifyCode',array('class'=>'form-control','style'=>'width: 150px;')); ?>
		<?php echo $form->error($model,'verifyCode'); ?>
         </div>
        </div>
      </div>
  
  
</div>
	<div class="row">
             <div class="col-md-8">
                  <div class="col-md-4">
		
	
		
                      
                  </div>
                  <div class="col-md-2">
		
            </div>
		</div>
                 
	
                 
         
            
           
	</div>
	<?php endif; ?>

	<div class="row buttons">
            <br>
            
		<?php echo CHtml::submitButton('Enviar' , array('class'=>'btn btn-primary')); ?>
	
            <br>
        </div>
    <br>
<?php $this->endWidget(); ?>

</div><!-- form -->

</div>

<?php endif; ?>


</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56757233-1', 'auto');
  ga('send', 'pageview');

</script>

