<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

/*
 * 
 <html< carousel bootstrap
 *   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

 
  
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/banner1.jpg"; ?>" alt="" >
      <div class="carousel-caption">
        
      </div>
    </div>
    <div class="item">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/bannerconst.jpg"; ?>" alt="" >
      <div class="carousel-caption">
     
      </div>
    </div>
      
        <div class="item">
        <img src="<?php echo Yii::app()->request->baseUrl."/images/banner_office.jpg"; ?>" alt="" >
      <div class="carousel-caption">
     
      </div>
    </div>
  
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
 * 
 */

?>
<div class="container" style=" margin-top: 100px; margin-bottom: 20px;">

    <div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Login</p>

	<div class="row">
              <div class="col-xs-3">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
        </div>

	<div class="row">
             <div class="col-xs-3">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>30,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div>
        </div>

   
             <div class="col-xs-3">
	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>
             </div>
 
            <br><br>
<div class="col-xs-4">
	<div class="row buttons">
		<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary')); ?>
	</div>
</div>
       

<?php $this->endWidget(); ?>
</div><!-- form -->
    



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56757233-1', 'auto');
  ga('send', 'pageview');

    
</script>

</div>