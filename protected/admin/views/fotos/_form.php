<?php
/* @var $this FotosController */
/* @var $model Fotos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fotos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_carro'); ?>
		<?php echo $form->textField($model,'id_carro'); ?>
		<?php echo $form->error($model,'id_carro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fotos_ruta'); ?>
		<?php echo $form->textField($model,'fotos_ruta',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'fotos_ruta'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->