<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="container" style=" margin-top: 70px; ">
    
    <div class="col-lg-12" style="margin-left: 100px;">
        <h2 style="margin-left: 200px;">Pasos para adquirir tu vehículo </h2>
    
    <br>
    
    <div class="row">
        
        <div class="col-lg-4 circle2">
            
             <img class="grow"  src="<?php echo Yii::app()->request->baseUrl."/images/pc.png"; ?>" style="margin-top: 2px;"  />
            
        </div>
        
        <div class="col-lg-8">
             <h4 style="text-align: justify"> Solicite su cotización a través de nuestros website, indicando sus datos de contacto,  marca y  modelo del vehículo que usted desea comprar. </h4>
        </div>
        
    </div>
    <br>
     <div class="row">
        
        <div class="col-lg-4 circle2">
            
             <img class="grow" src="<?php echo Yii::app()->request->baseUrl."/images/mails.png"; ?>" style="margin-top: 5px;" />
            
        </div>
        
        <div class="col-lg-8">
            <h4 style="text-align: justify">  Usted recibirá una respuesta de  su cotización via email <div class="alert alert-success col-lg-7" role="alert"> (carstogocorporation@gmail.com)</div></h4>
        </div>
        
    </div>
    
    <br>
    <div class="row">
        
        <div class="col-lg-4 circle2">
            
            <img class="grow"  src="<?php echo Yii::app()->request->baseUrl."/images/tel.png"; ?>" style="margin-top: -6px;"  />
            
        </div>
        
        <div class="col-lg-8">
            <h4 style="text-align: justify">  Si usted esta de acuerdo con el precio de la cotización enviada y desea realizar la negociación, debe llamarnos y tener el numero de cotización.</h4>
        </div>
        
    </div>
    
     <br>
    <div class="row">
        
        <div class="col-lg-4 circle2">
            
             <img class="grow" src="<?php echo Yii::app()->request->baseUrl."/images/fil.png"; ?>"  />
            
        </div>
        
        <div class="col-lg-8">
            <h4 style="text-align: justify">  Nosotros le enviaremos un acuerdo de pago estipulando el mutuo acuerdo de la negociación que será realizada en moneda local de los Estados Unidos (Dolares $), aceptamos transferencia electrónica  o cheque de gerencia de cuentas a su nombre, para así garantizar la seguridad en su compra.</h4>
        </div>
        
    </div>
       <br>
    <div class="row">
        
        <div class="col-lg-4 circle2">
            
             <img class="grow" src="<?php echo Yii::app()->request->baseUrl."/images/air.png"; ?>"  />
            
        </div>
        
        <div class="col-lg-8">
            <h4 style="text-align: justify">  Realizado el pago del vehículo, procederemos a enviarle los siguientes documentos: <br> Factura original a nombre del comprador.<br>
                Titulo de Propiedad Original endosado a nombre del comprador.<br>
                                                Cronograma Marítimo.
            </h4>
        </div>
        
    </div>
        <br>
    <div class="row">
        
        <div class="col-lg-4 circle2">
            
            <img class="grow"  src="<?php echo Yii::app()->request->baseUrl."/images/mano.png"; ?>" style="margin-top: -2px;"  />
            
        </div>
        
        <div class="col-lg-8">
            <h4 style="text-align: justify">  Si usted no cuenta con una agencia aduanas, nosotros le proporcionamos una cita, para que le asista en la nacionalización de su vehículo.
            </h4>
        </div>
        
    </div>
        <br>
    </div>
    
</div>

