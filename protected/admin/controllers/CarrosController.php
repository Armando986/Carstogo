<?php

class CarrosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','reqtest01'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','enable'),
				'users'=>array('admin'),
			),
			//array('deny',  // deny all users
		//		'users'=>array('*'),
		//	),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
           Yii::import('admin.vendor.*');
           require_once('phpimagen/php_image_magician.php');
           
		$model=new Carros;
                $model_foto=new Fotos;

           
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Carros']))
		{
			  $model->attributes=$_POST['Carros'];
                      
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto1');
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto2');
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto3');
                          $file_principal[]=CUploadedFile::getInstance($model, 'foto_principal');
                          
                         
                          $total=count($file);
                          $i=0;
                          $path=Yii::getPathOfAlias('webroot').'/images/';
                          
                          
                          if(!empty($file_principal[0])){
                          $model->foto_principal=$model->id_carro."_".$file_principal[0]->name;
                          $image=$model->id_carro."_".$file_principal[0]->name;
                         
                          $file_principal[0]->saveAs($path.$image);
                                            $foto_prin=new imageLib($path.$image);
                                            $foto_prin->resizeImage(545, 250, 'crop');
                                            $foto_prin->saveImage($path.$image);
                          }
                       if($model->save()){
                        
                           $model_foto->id_carro=$model->id_carro;
                              
                          
                            
                            while( $i < $total) {
                                
                                 if(!empty($file[$i])){
                                        $model_foto=new Fotos;
                                        $model_foto->id_carro=$model->id_carro;
                                        $model_foto->fotos_ruta=$model->id_carro."_".$file[$i]->name;
                                        $img=$model->id_carro."_".$file[$i]->name;
                                          
                                        $file[$i]->saveAs($path.$img);
                                        $model_foto->save();
                                            $foto_prin_1=new imageLib($path.$img);
                                            $foto_prin_1->resizeImage(350, 280, 0);
                                            $foto_prin_1->saveImage($path.$img);
                                          
                                 }
                             $i++; 
                            }
                          
                               $this->redirect(array('view','id'=>$model->id_carro));
                        }
                }
		        
		

		$this->render('create',array(
			'model'=>$model,
                        'model_foto'=>$model_foto,
		));
                
	
        }
        
        // Action borrar
          
            
         public function actionReqTest01($id) {
             
             
             $model=new Carros;
             
             
              if(isset($_POST['Carros'])){
                   $model->attributes=$_POST['Carros'];
                   $model->save();
              }
             // echo "id ".$id;
                
               $modelo=Fotos::model()->findAll(array('condition'=>'id_carro=:id_carro','params'=>array(':id_carro'=>$id)));
               // $modelo=FotoProducto::model()->findAll(array('condition'=>'catalogo_producto_id=:catalogo_producto_id','params'=>array(':catalogo_producto_id'=>$id)));
 
                   
                   //borra 
                   $total_ima=count($modelo);
                   
                   $dir=Yii::getPathOfAlias('webroot').'/images/';
                   
                   $num=0;
                   
                   while( $num < $total_ima ){
                       
                       if(is_file($dir.$modelo[$num]->fotos_ruta)){
                           unlink($dir.$modelo[$num]->fotos_ruta);
                       }
                       $num++;
                   }

                //borra image en base de datos
                   //FotoProducto::model()->deleteAll(array('condition'=>'catalogo_producto_id=:catalogo_producto_id','params'=>array(':catalogo_producto_id'=>$id)));
                   Fotos::model()->deleteAll(array('condition'=>'id_carro=:id_carro','params'=>array(':id_carro'=>$id)));
                   
                  
                  // CatalogoProducto::model()->deleteByPk($id);
                  
                  // $this->redirect(array('update','id'=>$model->id));
                  
               Yii::app()->end();
        }


        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
             Yii::import('admin.vendor.*');
             require_once('phpimagen/php_image_magician.php');
           
		$model=$this->loadModel($id);
                $model_foto=new Fotos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Carros']))
		{
			$model->attributes=$_POST['Carros'];
                        
                        //  $modelo=Fotos::model()->findAll(array('condition'=>'id_carro=:id_carro','params'=>array(':id_carro'=>$id)));
                        
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto1');
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto2');
                          $file[]=CUploadedFile::getInstance($model_foto, 'foto3');
                          $file_principal[]=CUploadedFile::getInstance($model, 'foto_principal');
                         
                          $path=Yii::getPathOfAlias('webroot').'/images/';
                          
                        //  $model->foto_principal='$file_principal->name';
                          if(!empty($file_principal[0])){
                          $model->foto_principal=$model->modelo." ".$file_principal[0]->name;
                          $image=$model->modelo." ".$file_principal[0]->name;
                          $file_principal[0]->saveAs($path.$image);
                                            $foto_prin=new imageLib($path.$image);
                                            $foto_prin->resizeImage(545, 250, 'crop');
                                            $foto_prin->saveImage($path.$image);
                          }
                           
                            $model_foto->id_carro=$model->id_carro;  
                            $total=count($file);
                            $i=0;
                        
                          
                           if($model->save()){
                          
                          while( $i < $total){
                              
                                        if(!empty($file[$i])){
                                        $model_foto=new Fotos;
                                        $model_foto->id_carro=$model->id_carro;
                                        $model_foto->fotos_ruta=$model->modelo." ".$file[$i]->name;
                                        $img=$model->modelo." ".$file[$i]->name;
                                        $file[$i]->saveAs($path.$img);
                                        $model_foto->save();
                                            $foto_prin_1=new imageLib($path.$img);
                                            $foto_prin_1->resizeImage(350, 280, 'crop');
                                            $foto_prin_1->saveImage($path.$img);
                                 }
                              
                              $i++;
                          }  
            
                         
                           
			
				$this->redirect(array('view','id'=>$model->id_carro));
		 }
                }
		$this->render('update', array(
			              'model'=>$model,
                                      'model_foto'=>$model_foto,
		));
	}

        
        
        
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		 $modelo=Fotos::model()->findAll(array('condition'=>'id_carro=:id_carro','params'=>array(':id_carro'=>$id)));
                 
                  //borra 
                   $total_ima=count($modelo);
                   
                   $dir=Yii::getPathOfAlias('webroot').'/images/';
                   
                   $num=0;
                   
                   while( $num < $total_ima ){
                       
                       if(is_file($dir.$modelo[$num]->fotos_ruta)){
                           unlink($dir.$modelo[$num]->fotos_ruta);
                       }
                       $num++;
                   }

                //borra image en base de datos
                   Fotos::model()->deleteAll(array('condition'=>'id_carro=:id_carro','params'=>array(':id_carro'=>$id)));
                   
                  
                   Carros::model()->deleteByPk($id);
                  
                  
                 
                  $this->redirect(array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
                $criteria=new CDbCriteria();
            
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=8;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('index',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Carros('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Carros']))
			$model->attributes=$_GET['Carros'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Carros the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Carros::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

        public function actionEnable($id){
            $model= Carros::model()->findByPk($id);
            if($model->estatus==1){
                $model->estatus=0;
            }
            else{
                $model->estatus=1;
            }
            
            $model->save();
            $this->redirect(array('index'));
        }
	/**
	 * Performs the AJAX validation.
	 * @param Carros $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='carros-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
