<?php
/* @var $this CarrosController */
/* @var $model Carros */


?>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imagen.css">

<script
   type="text/javascript" 
   src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js">
</script>

<!-- formulario -->
<script>
// this script executes when click on upload images link
    function uploadImage(num) {
        
   
        $('#archivos'+num).click();
       
    }
    
     function deleteImage(num) {

       $('#previews'+num).attr('src','blank');
        
       $('#previews'+num).hide();
    }
    
    
    

    
</script>



<div class="container" style=" margin-top: 70px;">
    
    
  <h1>Create Carros</h1>

  

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'carros-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',),
        'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

              <?php echo $form->errorSummary(array($model, $model_foto)); ?>

        <?php
        
         $nuevo=0;
         
         if(isset($model->id_carro)){
             
                $modelos=Fotos::model()->findAll('id_carro='.$model->id_carro); 
            
                $nuevo = count($modelos);
      
         }
         ?>
        
    
   
           <div class="col-xs-6 col-md-3">
                	<?php if(isset($model)){ ?>
                <div class="preview_prod">

                      <div id="loading4">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                    <div class="step2">
                        <?php 
                                    if( isset($model)){
                                        ?>
                                          <img src="<?php echo Yii::app()->request->baseUrl.'/images/'.$model->foto_principal;?>"   id="previews4" style="display: block;" width="150px" height="150px"/>  
                                     <?php
                                    }
                            else{
                                ?>
                                    <img src="#" id="previews4"  width="150px" height="150px"/>         
                             <?php
                            }
                            ?>
                    </div>
                     <?php // echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->archivo,"archivo",array("width"=>200, "height"=>200)); ?> 
                 </div>
                 <?php } 

                 else { ?>
                <div class="preview_prod">

                        
                        <div id="loading4">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                        
                         <div class="step2">
                           
                                    <img src="#" id="previews4"  width="150px" height="150px"/>         
                           
                            
                          </div>
                           
                          

                 </div>
                
              

                 <?php 
                }
                 ?>

	<div class="row" style="display: none">
            <div class="col-md-6">
		<?php echo $form->labelEx($model,'foto_principal'); ?>
		<?php echo $form->fileField($model,'foto_principal',array('id'=>'archivos4', 'class'=>'input-file')); ?>
		<?php echo $form->error($model,'foto_principal'); ?>
             
            </div>
           
	</div>
                  <?php echo CHtml::button("Agregar",array('title'=>"Edit",'id'=>'archivos','onclick'=>'return uploadImage(4);','class'=>'btn btn-primary')); ?>
                 
                
             <?php
             if( isset($model)){
                  echo CHtml::ajaxLink(
                            'Borrar',          // the link body (it will NOT be HTML-encoded.)
                            array('Carros/reqTest01', 'id'=>$model->id_carro),
                            array(  
                                 'success'=>'function(html){ $("#previews4").attr("src","blank");  $("#previews4").hide();}',
                                ),
                            array('confirm'=>'Esta seguro que desea borrar?','class'=>'btn btn-danger',)
                                    
                                );
             }
                ?>

             
             

           
            

             
             

            </div>
        
	<div class="row">
            <div class="col-xs-4">
		<?php echo $form->labelEx($model,'modelo'); ?>
		<?php echo $form->textField($model,'modelo',array('size'=>80,'maxlength'=>60, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'modelo'); ?>
            </div>
	</div>

	<div class="row">
            <div class="col-xs-4">
		<?php echo $form->labelEx($model,'marca'); ?>
		<?php echo $form->textField($model,'marca',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'marca'); ?>
            </div>
	</div>
        
        <div class="row">
            <div class="col-xs-4">
		<?php echo $form->labelEx($model,'ano'); ?>
		<?php echo $form->textField($model,'ano',array('size'=>30,'maxlength'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'ano'); ?>
            </div>
	</div>

	<div class="row">
            <div class="col-xs-4">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->textField($model,'precio',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'precio'); ?>
            </div>
	</div>



	<div class="row">
            <div class="col-xs-1">
		<?php echo $form->labelEx($model,'lado'); ?>
		<?php echo $form->textField($model,'lado',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'lado'); ?>
            </div>
            
        
	</div>
        
        <div class="row">
            <div class="col-xs-1">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'tipo'); ?>
            </div>
            
        
	</div>
        
        <div class="row">
            <div class="col-xs-1">
		<?php echo $form->labelEx($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>50,'maxlength'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'estatus'); ?>
            </div>
            
        
	</div>
        
        <div class="row">
            
            
                <div class="col-xs-6 col-md-3">
                	<?php if($nuevo!=0){ ?>
                <div class="preview_prod">

                      <div id="loading1">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                    <div class="step2">
                        <?php 
                                    if( isset($modelos[0])){
                                        ?>
                                          <img src="<?php echo Yii::app()->request->baseUrl.'/images/'.$modelos[0]->fotos_ruta; ?>" id="previews1" style="display: block;" width="150px" height="150px"/>  
                                     <?php
                                    }
                            else{
                                ?>
                                    <img src="#" id="previews1"  width="150px" height="150px"/>         
                             <?php
                            }
                            ?>
                    </div>
                     <?php // echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->archivo,"archivo",array("width"=>200, "height"=>200)); ?> 
                 </div>
                 <?php } 

                 else { ?>
                <div class="preview_prod">

                        
                        <div id="loading1">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                        
                         <div class="step2">
                           
                                    <img src="#" id="previews1"  width="150px" height="150px"/>         
                           
                            
                          </div>
                           
                          

                 </div>
                
              

                 <?php 
                }
                 ?>

	<div class="row" style="display: none">
            <div class="col-md-6">
		<?php echo $form->labelEx($model_foto,'foto1'); ?>
		<?php echo $form->fileField($model_foto,'foto1',array('id'=>'archivos1', 'class'=>'input-file')); ?>
		<?php echo $form->error($model_foto,'foto1'); ?>
             
            </div>
           
	</div>
                  <?php echo CHtml::button("Agregar",array('title'=>"Edit",'id'=>'archivos','onclick'=>'return uploadImage(1);','class'=>'btn btn-primary')); ?>
                 
                
             <?php
             if( isset($modelos[0])){
                  echo CHtml::ajaxLink(
                            'Borrar',          // the link body (it will NOT be HTML-encoded.)
                            array('Carros/reqTest01', 'id'=>$modelos[0]->id_carro),
                           
                            array(  
                                  'success'=>'function(html){ $("#previews1").attr("src","blank");  $("#previews1").hide();}',
                               
                                ),
                            array('confirm'=>'Esta seguro que desea borrar?','class'=>'btn btn-danger',)
                                    
                                );
             }
                ?>

             
             

            </div>
          
	</div>
         <div class="row">
            
            
                <div class="col-xs-6 col-md-3">
                	<?php if($nuevo!=0){ ?>
                <div class="preview_prod">

                      <div id="loading2">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                    <div class="step2">
                        <?php 
                                    if( isset($modelos[1])){
                                        ?>
                                          <img src="<?php echo Yii::app()->request->baseUrl.'/images/'.$modelos[1]->fotos_ruta; ?>" id="previews2" style="display: block;" width="150px" height="150px"/>  
                                     <?php
                                    }
                            else{
                                ?>
                                    <img src="#" id="previews2"  width="150px" height="150px"/>         
                             <?php
                            }
                            ?>
                    </div>
                     <?php // echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->archivo,"archivo",array("width"=>200, "height"=>200)); ?> 
                 </div>
                 <?php } 

                 else { ?>
                <div class="preview_prod">

                        
                        <div id="loading2">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                        
                         <div class="step2">
                           
                                    <img src="#" id="previews2"  width="150px" height="150px"/>         
                           
                            
                          </div>
                           
                          

                 </div>
                
              

                 <?php 
                }
                 ?>

	<div class="row" style="display: none">
            <div class="col-md-6">
		<?php echo $form->labelEx($model_foto,'foto2'); ?>
		<?php echo $form->fileField($model_foto,'foto2',array('id'=>'archivos2', 'class'=>'input-file')); ?>
		<?php echo $form->error($model_foto,'foto2'); ?>
             
            </div>
           
	</div>
                  <?php echo CHtml::button("Agregar",array('title'=>"Edit",'id'=>'archivos','onclick'=>'return uploadImage(2);','class'=>'btn btn-primary')); ?>
                 
                
             <?php
             if( isset($modelos[1])){
                  echo CHtml::ajaxLink(
                            'Borrar',          // the link body (it will NOT be HTML-encoded.)
                            array('Carro/reqTest01', 'id'=>$modelos[1]->id_carro),
                           
                            array(  
                                     
                                     'success'=>'function(html){ $("#previews2").attr("src","blank");  $("#previews2").hide();}',
                               
                                ),
                            array('confirm'=>'Esta seguro que desea borrar?','class'=>'btn btn-danger',)
                                    
                                );
             }
                ?>

             
             

            </div>
          
	</div>
         <div class="row">
            
            
                <div class="col-xs-6 col-md-3">
                	<?php if($nuevo!=0){ ?>
                <div class="preview_prod">

                      <div id="loading3">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                    <div class="step2">
                        <?php 
                                    if( isset($modelos[2])){
                                        ?>
                                          <img src="<?php echo Yii::app()->request->baseUrl.'/images/'.$modelos[2]->fotos_ruta; ?>" id="previews3" style="display: block;" width="150px" height="150px"/>  
                                     <?php
                                    }
                            else{
                                ?>
                                    <img src="#" id="previews3"  width="150px" height="150px"/>         
                             <?php
                            }
                            ?>
                    </div>
                     <?php // echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->archivo,"archivo",array("width"=>200, "height"=>200)); ?> 
                 </div>
                 <?php } 

                 else { ?>
                <div class="preview_prod">

                        
                        <div id="loading3">
                           <img id="loading-image" src="<?php echo Yii::app()->request->baseUrl.'/css/images/loader.gif'?>" alt="Loading..." />
                        </div>
                        
                         <div class="step2">
                           
                                    <img src="#" id="previews3"  width="150px" height="150px"/>         
                           
                            
                          </div>
                           
                          

                 </div>
                
              

                 <?php 
                }
                 ?>

	<div class="row" style="display: none">
            <div class="col-md-6">
		<?php echo $form->labelEx($model_foto,'foto3'); ?>
		<?php echo $form->fileField($model_foto,'foto3',array('id'=>'archivos3', 'class'=>'input-file')); ?>
		<?php echo $form->error($model_foto,'foto3'); ?>
             
            </div>
           
	</div>
                  <?php echo CHtml::button("Agregar",array('title'=>"Edit",'id'=>'archivos','onclick'=>'return uploadImage(3);','class'=>'btn btn-primary')); ?>
                 
                
             <?php
             if( isset($modelos[2])){
                  echo CHtml::ajaxLink(
                            'Borrar',          // the link body (it will NOT be HTML-encoded.)
                            array('Carros/reqTest01', 'id'=>$modelos[2]->id_carro),
                           
                            array(  
                                     
                                     'success'=>'function(html){ $("#previews3").attr("src","blank");  $("#previews3").hide();}',
                               
                                ),
                            array('confirm'=>'Esta seguro que desea borrar?','class'=>'btn btn-danger',)
                                    
                                );
             }
                ?>

             
             

            </div>
          
	</div>
        
       
       
        
        
        <br>
	<div class="row buttons">
             <div class="col-xs-1">
		<?php echo CHtml::submitButton($model_foto->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-primary')); ?>
	
             </div>
        </div>
        
 <?php Yii::app()->getClientScript()->registerScript("ejemplo_ajax",
"

	  $('#archivos1').change(function() {
         
          
              readURL(this,'#previews1',1);
              
           
          });
          
           $('#archivos2').change(function() {
         

               readURL(this,'#previews2',2);
           
          });
          

          $('#archivos3').change(function() {
         

                readURL(this,'#previews3',3);
           
          });
          

         $('#archivos4').change(function() {
         

              readURL(this,'#previews4',4);
           
          });
          
      


        function readURL(input, target, num) {
        
            if(!/(\.bmp|\.gif|\.jpg|\.jpeg|\.png)$/i.test(input.value)){
                alert('Archivo invalido....');
                document.getElementById('previews'+num).value='';
                return false;
            }

            
 
            if (input.files && input.files[0]) {
            
               var reader = new FileReader();
               var image_target=$(target);
               
             
             

               reader.onload = function(e) {
               
                
                     image_target.fadeIn(500);
        
                     image_target.attr('src', e.target.result).show();
 

                 
                  
               };

               reader.readAsDataURL(input.files[0]);
           }
       }
       



",CClientScript::POS_LOAD); ?>
        
        
        <?php
//Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerScript("loadchild","

", CClientScript::POS_LOAD);
?>
               
        
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
