<?php
/* @var $this FotosController */
/* @var $data Fotos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_foto')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_foto), array('view', 'id'=>$data->id_foto)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_carro')); ?>:</b>
	<?php echo CHtml::encode($data->id_carro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fotos_ruta')); ?>:</b>
	<?php echo CHtml::encode($data->fotos_ruta); ?>
	<br />


</div>