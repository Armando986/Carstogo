<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}
        
        
        //mensaje
        	public function actionMensaje()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('mensaje');
	}
        
        
           //mensaje
        	public function actionPasos()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('pasos');
	}
        
        
        
        
        
        
        //contactos
            	public function actionContactos()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('contactos');
	}
        
         //rusticos
         public function actionRustico(){
                 $criteria=new CDbCriteria();
             $criteria->select=array("*");
                $criteria->condition='tipo="rustico" AND estatus=1';
           
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('rustico',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
        }
       
        //usados
         public function actionUsados(){
                 $criteria=new CDbCriteria();
             $criteria->select=array("*");
                $criteria->condition='tipo="usados" AND estatus=1';
           
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('usados',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
        }
        
        //coupe
         public function actionCoupe(){
                 $criteria=new CDbCriteria();
             $criteria->select=array("*");
                $criteria->condition='tipo="coupe" AND estatus=1';
           
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('coupe',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
        }
        
        
        //sedanes
        
        public function actionSedan(){
                 $criteria=new CDbCriteria();
             $criteria->select=array("*");
                $criteria->condition='tipo="sedan" AND estatus=1';
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('sedan',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
        }
        //sport wagon
        public function actionSportwagon(){
              $criteria=new CDbCriteria();
                $criteria->select=array("*");
                $criteria->condition='tipo="sportwagon" AND estatus=1';
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('sportwagon',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
          
        }
        //pibkup
        //sport wagon
        public function actionPickup(){
             $criteria=new CDbCriteria();
                $criteria->select=array("*");
                $criteria->condition='tipo="pickup" AND estatus=1';
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('pickup',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
      
        }
        //van
        //sport wagon
        public function actionVans(){
           $criteria=new CDbCriteria();
                $criteria->select=array("*");
                $criteria->condition='tipo="van" AND estatus=1';
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('vans',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
            
        }
        //camion
        //sport wagon
        public function actionCamion(){
              $criteria=new CDbCriteria();
                $criteria->select=array("*");
                $criteria->condition='tipo="camion" AND estatus=1';
                $count= Carros::model()->count($criteria);
                $pages=new CPagination($count);
    
                //$model=  Usuarios::model();
                //$usuarios=$model->findAll();
                
                // results per page
                $pages->pageSize=6;
                $pages->applyLimit($criteria);
                $carros=  Carros::model()->findAll($criteria);
		$this->render('camion',array(
			
                    'carros'=>$carros,
                    'pages'=>$pages,
		));
           
        }
        
         //sport wagon
        public function actionGaleria(){
            $this->render('galeria');
        }
        

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->nombre).'?=';
				$subject= '=?UTF-8?B?'.base64_encode(strtoupper($model->vehiculo)).'?=';
				/*
                                $headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";
                                */
                                $message="COTIZACION A NOMBRE DE: <b>".$model->nombre."</b><br><br>"            
                                        . "PUERTO: <b>".$model->puerto."</b><br><br><br>"
                                        . "EMAIL: <b>".$model->email."</b><br><br><br>"
                                        . "TELEFONO: <b>".$model->telefono."</b><br><br><br>"
                                        . $model->mensaje; // 
                                
                               // $correo_cliente=trim($model->email);

                                $mail = new EnviarMail();
                                
                             
                                  
                                $mail->enviar( array(Yii::app()->params['adminEmail'],Yii::app()->name),
                                               array(Yii::app()->params['adminEmail'],Yii::app()->name),
                                               array($model->email, $name),
                                               $subject, 
                                               $message);
        
                                 
                                $mail->enviar( array("info@carstogo.com.ve","CAR & CARS TO GO"),
                                               array("carstogocorporation@gmail.com","CAR & CARS TO GO"),
                                               array(trim($model->email), $name),
                                               "COTIZACION  ".$subject, 
                                               $message);
                                /*
                                 $mail->enviar( array("info@carstogo.com.ve","CAR & CARS TO GO"),
                                               array("armandozabala@gmail.com","ARMANDO ZABALA"),
                                               array(trim($model->email), $name),
                                               "COTIZACION  ".$subject, 
                                               $message);
                                 
                                 */
                              
                                /*
                                $cabeceras1="From: CARS & CARS TO GO CORPORATION | Exportación de Vehículos <info@carstogo.com.ve>\n";
                                $cabeceras1.="Reply-To: {$model->email} \n";
                                $cabeceras1.="MIME-version: 1.0\n";
                                $cabeceras1.="Content-type: text/html; charset=iso-8859-1\n";
                                mail(Yii::app()->params['adminEmail'], 
                                     $titulo_cliente, 
                                     $mensaje_cliente,
                                     $cabeceras1);
                                 * */
                                
                              /*  
                                $mails = new EnviarMail();
                                $mails->enviar(array(Yii::app()->params['adminEmail'],Yii::app()->name),
                                               array($model->email, Yii::app()->name),
                                               'text',
                                               'texto2');
                               * */
                              
                                 //envio de correo al cliente responder
                             /*
                                $correo_cliente=trim($model->email);
                                $titulo_cliente=stripslashes($subject);
                                $mensaje_cliente=stripslashes($message);
                                
                                $cabeceras="From: CARS & CARS TO GO CORPORATION | Exportación de Vehículos <info@imporauto.net> \n";
                                $cabeceras.="Reply-To: ".$correo_cliente." \n";
                                $cabeceras.="MIME-version: 1.0\n";
                                $cabeceras.="Content-type: text/html; charset=iso-8859-1\n";
                                mail($correo_cliente, $titulo_cliente, $mensaje_cliente,$cabeceras);
                               */
                             
                                /*
                                if($mail_cliente){
                                    
                                    
                                }
                              * */
                              
                                     //$this->redirect('index.php?r=site/mensaje');
                                
                                      Yii::app()->user->setFlash('contact','<h2> Gracias por contactarnos. </h2> <br> <h4> Nosotros responderemos tu solicitud a la brevedad posible. </h4> <br><br><br>');
				
                                     $this->refresh();
                               

                                 
                             
                                 
				//mail(Yii::app()->params['adminEmail'],$subject,$model->mensaje,$headers);
				/*
                                $nombre="Cars and Cars to Go Corporation";
                                $mails="info@imporauto.net";
                                $titulo="Gracias por contactarnos";
                                 
                                $mensaje='Saludos  Sr/Sra'.$name.'<br> Gracias por contactarnos. Hemos recibido su mensaje, estaremos respondiendo su solicitud a la brevedad posible  <br><br> Saludos     ';
                                
                                $headers_cliente="From: $nombre <{$mails}>\r\n".
					"Reply-To: {$mails}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";
                                        
                                mail($model->email,$titulo,$mensaje,$headers_cliente);
                                */
                                /*
                                $nombre="Cars and Cars to Go Corporation";
                                $mails=$model->email;
                                $titulo="Gracias por contactarnos";
                                 
                                $nombrem=  strtoupper($model->nombre);
                                
                                $mensaje='Hola  Sr/Sra  '.$nombrem.'<br><br> Gracias por contactarnos. Hemos recibido su mensaje, estaremos respondiendo su solicitud a la brevedad posible  <br><br> Saludos     ';
                                
                                $mail->enviar( array(Yii::app()->params['adminEmail'],Yii::app()->name),
                                               array($mails),
                                               $titulo, 
                                               $mensaje);
                                */
                             
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}