<?php
//Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$backend=dirname(dirname(__FILE__));
$frontend=dirname($backend);
Yii::setPathOfAlias('admin', $backend);


return array(
	 'basePath'=>$frontend,
         'controllerPath' => $backend.'/controllers',
         'viewPath' => $backend.'/views',
         'runtimePath' => $backend.'/runtime',
         'theme'=>'backend',
	
         'preload'=>array('log'),
	// autoloading model and component classes
	'import'=>array(
		'admin.models.*',
                'admin.components.*',
                'admin.extensions.*',
                'admin.vendor.*',
                //'application.models.*',
		//'application.components.*',
	),

	'modules'=>array(
               
              
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
                    'generatorPaths'=>array(
                        'boostrap.gii',
                    ),
			'class'=>'system.gii.GiiModule',
			'password'=>'1234',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
              
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
	
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
               
			'rules'=>array(
			    'admin'=>'site/index',
                            'admin/<_c>'=>'<_c>',
                            'admin/<_c>/<_a>'=>'<_c>/<_a>',	
			),
		),

            /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		*/
            
		'db'=>array(
			'connectionString' =>     'mysql:host=localhost;dbname=sjdndqwa_cctgcorp',
			'emulatePrepare' => true,
			'username' => 'sjdndqwa_root',
			'password' => 'cctgcorp2014',
			'charset' => 'utf8',
		),

		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'info@carstogo.com.ve',
            
	),
);