<?php
/* @var $this FotosController */
/* @var $model Fotos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_foto'); ?>
		<?php echo $form->textField($model,'id_foto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_carro'); ?>
		<?php echo $form->textField($model,'id_carro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fotos_ruta'); ?>
		<?php echo $form->textField($model,'fotos_ruta',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->