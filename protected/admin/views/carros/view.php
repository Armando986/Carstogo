<?php
/* @var $this CarrosController */
/* @var $model Carros */

$this->breadcrumbs=array(
	'Carroses'=>array('index'),
	$model->id_carro,
);


?>

<div class="container" style=" margin-top: 70px;">

<h1>Carro #<?php echo $model->id_carro; ?></h1>

<div style='margin-left: 20px;'>
    

<div class="row">
    
    <h4>  <?php echo "Marca:  ". $model->marca; ?> </h4> 
    
</div>

<div class="row">
    
    <h4>  <?php echo "Modelo:  ".  $model->modelo; ?></h4>
    
</div>
    
    <div class="row">
    
        <h4>  <?php echo "Año:  ".  $model->ano; ?></h4>
    
</div>
    
        <div class="row">
    
            <h4>   <?php echo "Precio:  ".  $model->precio; ?> </h4>
    
</div>
    <br>
</div>
    <div class="preview_prod">

                     
                    <div class="step2">
                        <?php 
                                    if( isset($model->foto_principal)){
                                        ?>
                                          <img src="<?php echo '../../images/'.$model->foto_principal; ?>" id="previews1" style="display: block;" width="150px" height="150px"/>  
                                     <?php
                                    }
                            else{
                                ?>
                                    <img src="#" id="previews1"  width="150px" height="150px"/>         
                             <?php
                            }
                            ?>
                    </div>
                     <?php // echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$model->archivo,"archivo",array("width"=>200, "height"=>200)); ?> 
                 </div>


<br>
<br>

<?php echo CHtml::link('Lista de Carro',array('/carros/index'),array('class'=>'btn btn-warning btn-lg')); ?>


<?php echo CHtml::link('Nuevo Carro',array('/carros/create'),array('class'=>'btn btn-success btn-lg')); ?>

<?php echo CHtml::link("Delete", '#', array('class'=>'btn btn-danger btn-lg', 'submit'=>array('carros/delete', "id"=>$model->id_carro),  'confirm' => 'Are you sure you want to delete?'));

?>

<br><br><br>

<?php  /* $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_carro',
		'modelo',
		'marca',
		'precio',
		'foto_principal',
		'lado',
	),

)); * */ ?>

</div>