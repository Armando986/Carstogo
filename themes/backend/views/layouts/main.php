<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="construccion, venezuela, musica, maquinarias, tractores, carros, camionetas, tucarro, mujeres, compañia, contratista, pdvsa">
        <meta http-equiv="keywords" content="construccion, venezuela, musica, maquinarias, tractores, carros, camionetas, tucarro, mujeres, compañia, contratista, pdvsa, chevron">
        <meta name="language" content="en" />

	<!-- blueprint CSS framework -->
        <!--  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" /> -->
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" /> 
        
        
         <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/magnific-popup.css"> 
             <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animation.css"> 

               
        <!-- jQuery 1.7.2+ or Zepto.js 1.0+ -->
  

  
        <link type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/skitter.styles.css" media="all" rel="stylesheet" />

        
        
        
        
       
        
       
        
        
	<title><?php echo "Administracion" ?></title>
        <style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */

      html,
      body{
          color: white;
      }
      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -60px;
      }

      /* Set the fixed height of the footer here */
      #push,
      #footer {
        height: 60px;
      }
      #footer {
        background-color: #f5f5f5;
        color:  #313131;
      }

      /* Lastly, apply responsive CSS fixes as necessary */
      @media (max-width: 767px) {
        #footer {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
      }



      /* Custom page CSS
      -------------------------------------------------- */
      /* Not required for template or sticky footer method. */

      #wrap > .container {
        padding-top: 60px;
      }
      .container .credit {
        margin: 20px 0;
      }

      code {
        font-size: 80%;
      }
      
     

    </style>
    
    <link rel="icon"  href="<?php echo Yii::app()->request->baseUrl."/images/favicon/animated.gif"; ?>" type="image/gif"  />

    
</head>

<body>

   
   
    <nav class="navbar navbar-default navbar-fixed-top" style=" background: rgba(27,45,74,1);
background: -moz-linear-gradient(top, rgba(27,45,74,1) 0%, rgba(57,90,173,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(27,45,74,1)), color-stop(100%, rgba(57,90,173,1)));
background: -webkit-linear-gradient(top, rgba(27,45,74,1) 0%, rgba(57,90,173,1) 100%);
background: -o-linear-gradient(top, rgba(27,45,74,1) 0%, rgba(57,90,173,1) 100%);
background: -ms-linear-gradient(top, rgba(27,45,74,1) 0%, rgba(57,90,173,1) 100%);
background: linear-gradient(to bottom, rgba(27,45,74,1) 0%, rgba(57,90,173,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1b2d4a', endColorstr='#395aad', GradientType=0 );" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a id="logo"  style=" margin-left: 0px;" href="<?php echo Yii::app()->homeUrl;?>"><img class=" fadeIn" src="<?php echo Yii::app()->request->baseUrl."/images/TOPiconblanco.png"; ?>"/></a>
    </div>

    

        
        
        
    <ul class="nav navbar-nav navbar-right" style=" margin-top: 10px;">
        
          
          
                
              
 
          
          <form class="navbar-form navbar-left" role="search">
        
         <?php echo CHtml::link('Principal',array('/site/index'),array('class'=>'btn btn-primary')); ?>
                <?php echo CHtml::link('Salir',array('site/logout'),array('class'=>'btn btn-primary')); ?>
      </form>
     
          
         
       
      </ul>
   
  </div><!-- /.container-fluid -->
</nav>
   
<div class="container" id="page">
   
	<?php echo $content; ?>

	<div class="clear"></div>

        </div><!-- page -->
        <div id="footer" style="background: rgba(55,95,189,1);
background: -moz-linear-gradient(top, rgba(55,95,189,1) 0%, rgba(19,28,41,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(55,95,189,1)), color-stop(100%, rgba(19,28,41,1)));
background: -webkit-linear-gradient(top, rgba(55,95,189,1) 0%, rgba(19,28,41,1) 100%);
background: -o-linear-gradient(top, rgba(55,95,189,1) 0%, rgba(19,28,41,1) 100%);
background: -ms-linear-gradient(top, rgba(55,95,189,1) 0%, rgba(19,28,41,1) 100%);
background: linear-gradient(to bottom, rgba(55,95,189,1) 0%, rgba(19,28,41,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#375fbd', endColorstr='#131c29', GradientType=0 );">
            <div class="container" style=" color: white;">
          
           <p class="muted credit">
     
                Copyright &copy; <?php echo date('Y'); ?>
		
		
                </p>
      </div>
    </div>
	

         <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.js" ></script>


        



     



           
        </script>
       
</body>
        
</html>
