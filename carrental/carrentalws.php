<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

//echo'Hello World!';

$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json');
$app->get('/download/:name', function ($name) {    	
	$entireFileStr = file_get_contents("Sync/".$name.".txt");
	echo $entireFileStr;
});

$app->post('/upload/:user_id/:client_name', function($user_id,$client_name) use ($app) {
	$app->response()->header("Content-Type", "application/json");
	//move_uploaded_file($_FILES["file"]["tmp_name"], 'folders/' . $id . '/' . $_FILES["file"]["name"]);
	move_uploaded_file($_FILES["file"]["tmp_name"], 'UploadedFiles/'. $_FILES["file"]["name"]);
	
	$app->response()->header("Content-Type", "text/html");
	//INCLUDE SUCCESS PAGE
	//include "success.html";
	echo 'Success... param1='.$user_id.' param2='.$client_name;
});

$app->get('/files/:id/', function($id) use ($app) {
	echo 'Hello GET files/' . $id;
});

$app->run();
?>
