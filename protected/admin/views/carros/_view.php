<?php
/* @var $this CarrosController */
/* @var $data Carros */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_carro')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_carro), array('view', 'id'=>$data->id_carro)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modelo')); ?>:</b>
	<?php echo CHtml::encode($data->modelo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marca')); ?>:</b>
	<?php echo CHtml::encode($data->marca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto_principal')); ?>:</b>
	<?php echo CHtml::encode($data->foto_principal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lado')); ?>:</b>
	<?php echo CHtml::encode($data->lado); ?>
	<br />


</div>